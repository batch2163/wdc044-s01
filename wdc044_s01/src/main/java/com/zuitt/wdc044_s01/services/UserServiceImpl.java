package com.zuitt.wdc044_s01.services;
import com.zuitt.wdc044_s01.models.User;
import com.zuitt.wdc044_s01.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service

public class UserServiceImpl implements UserService {
    @Autowired
    private  UsersRepository usersRepository;

    public void createUser(User user){
        usersRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(usersRepository.findByUsername(username));
    }


}
