package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.config.JwtToken;
import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import com.zuitt.wdc044_s01.repositories.PostsRepository;
import com.zuitt.wdc044_s01.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostsRepository postRepository;

    @Autowired
    private UsersRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();

        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);

    }

    public ResponseEntity updatePost(Long id, String stringToken, Post post){
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deletePost(Long id, String stringToken){
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new  ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }


    public Iterable<Post> allUserPost(String stringToken) {

       User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        return author.getPosts();
    }


    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }
}
