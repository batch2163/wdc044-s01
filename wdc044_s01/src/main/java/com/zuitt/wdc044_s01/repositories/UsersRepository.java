package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface UsersRepository extends CrudRepository<User, Object> {
    User findByUsername(String username);

}
