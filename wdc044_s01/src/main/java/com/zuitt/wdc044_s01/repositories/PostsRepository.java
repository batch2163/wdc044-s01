package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository //an interface marked as @Repository - contains methods for the database manipulation

//by extending CrudRepository, PostRepository has inherited its pre-defined methods for crud records
public interface PostsRepository extends CrudRepository<Post, Object> {
    Iterable<Post> findAll();
    //an interface contains behavior that a class implements



}
