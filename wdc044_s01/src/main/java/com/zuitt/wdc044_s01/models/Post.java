package com.zuitt.wdc044_s01.models;


import javax.persistence.*;

//mark this Java object as a presentation of a database via @Entity
@Entity

//designated table name
@Table(name = "posts")

public class Post {

    //indicate that this property represents the primary key
    @Id
    //values for this property will be auto-increment using @GeneratedValue
    @GeneratedValue
    private Long id;

    //class properties that represents table column in a relational databases are annotated as @Column

    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;


    //Empty constructor
    public Post(){}

        //parameterized constructor
        public Post(String title, String content){
            this.title = title;
            this.content = content;
        }

        //getters / setters

    public String getTitle(){
        return title;
    }
    public String getContent(){
        return content;
    }

    public void setContent(String content){

        this.content = content;
    }

    public void setTitle(String title){

        this.title = title;
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }





}
